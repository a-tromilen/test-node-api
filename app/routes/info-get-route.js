const express = require('express');
const helper = require('../common/commonHelper');
const logger = require('../loggers/logger');
const GetInfoController = require('../controllers/get-info-controller');

const router = express.Router();

function handler(request, response) {
  const idParam = request.params.id;
  if (idParam === undefined || idParam === null) {
    return helper.setResponseWithError(response, 403, 'Invalid parameters');
  }

  let toReturn;
  const getInfoController = new GetInfoController();
  getInfoController.getInfoById(idParam)
        .then((result) => {
          toReturn = helper.setResponse(response, result);
        }).catch((error) => {
          logger.error(`Error en respuesta de controller. Parámetro entreado por url: ${idParam}`);
          toReturn = helper.setResponseWithError(response, 500, error);
        });
  return toReturn;
}

router.get('/info/:id', handler);

module.exports = router;
module.exports.handler = handler;
