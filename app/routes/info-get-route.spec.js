var mockery = require('mockery');
var httpMocks = require('node-mocks-http');
var EventEmitter = require("events").EventEmitter;

describe('infoGet route', () => {
  beforeEach(() => {
    mockery.enable({
      warnOnReplace: false,
      warnOnUnregistered: false,
      useCleanCache: true
    });
    mockery.registerMock('../../loggers/logger', {
      debug: function () {}, 
      error: function() {}, 
      info: function() {}
    });
  });

  afterEach(() => {
    mockery.disable();
    mockery.deregisterAll();
  });

  describe('Test Handler Function', () => {
    it('should not have errors', () => {
      // mock del controller
      class GetInfoControllerMock {
        getInfoById(id) {
          return new Promise((resolve, reject) => {
            resolve({field1:"ok", field2: "ok"});
          });
        }
      }

      mockery.registerMock("../controllers/get-info-controller", GetInfoControllerMock);

        // mock del request
        let request = httpMocks.createRequest({
            method: 'GET',
            url: "/info/:id",
            params: {id: '1'},
            headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"},
        });

      // mock del response
      let response = httpMocks.createResponse({
        eventEmitter: EventEmitter
      });

      // invocar a la ruta
      route = require("./info-get-route");
      route.handler(request, response);

      expect(response.statusCode).toBe(200);
    });

    it('should return error when id parameter is not received', () => {
      // mock del controller
      class GetInfoControllerMock {
        getInfoById(id) {
          return new Promise((resolve, reject) => {
              reject("error");
          });
        }
      }

      mockery.registerMock("../controllers/get-info-controller", GetInfoControllerMock);

      // mock del request
        let request = httpMocks.createRequest({
            method: 'GET',
            url: "/info/:id",
            params: {},
            headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"},
        });

      // mock del response
      let response = httpMocks.createResponse({
        eventEmitter: EventEmitter
      });

      // invocar a la ruta
      route = require("./info-get-route");
      route.handler(request, response);

      expect(response.statusCode).toBe(403);
      expect(response._getData().message).toBe("Invalid parameters");
    });

    it('should return error when controller throws error', () => {
        // mock del controller
        class GetInfoController {
            getInfoById(id) {
                return new Promise((resolve, reject) => {
                    resolve("error");
                });
            }
        }

        mockery.registerMock("../controllers/get-info-controller", GetInfoController);

        // mock del request
        let request = httpMocks.createRequest({
            method: 'GET',
            url: "/info/:id",
            params: {
                id: 'asd'
            },
            headers: {"x-access-token": "M2UzOTUxZjNkZWYwNGQzMzJiYWJlYTE4d"},
        });

        // mock del response
        let response = httpMocks.createResponse({
            eventEmitter: EventEmitter,
        });

        // invocar a la ruta
        route = require("./info-get-route");
        route.handler(request, response);

        expect(response.statusCode).toBe(200);

    });
  });
});