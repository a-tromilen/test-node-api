const InfoMongoService = require('../services/persistence/info-mongo-service');

module.exports = class GetInfoController {
  getInfoById(id) {
    const infoMongoService = new InfoMongoService();
    return new Promise((resolve, reject) => {
      infoMongoService.findById(id)
      .then((response) => {
        resolve(response);
      })
      .catch((error) => {
        reject(error);
      });
    });
  }
};
